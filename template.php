<?php

function royal_preprocess_node(&$vars, $hook){
  
  // Working some magic to get separate vars for the comment data
  if ($vars['node']->links['comment_comments']) {
    $vars['comment_link'] = t('This post has '); 
    $vars['comment_link'] .= l($vars['node']->links['comment_comments']['title'], $vars['node']->links['comment_comments']['href']);
  }
  
  if ($vars['node']->links['comment_add']) {
    $vars['add_comment_link'] = l($vars['node']->links['comment_add']['title'], $vars['node']->links['comment_add']['href']);     
  }
  
  if ($vars['node']->links['node_read_more']) {
    $vars['read_more_link'] = l($vars['node']->links['node_read_more']['title'], $vars['node']->links['node_read_more']['href']);             
  }
}