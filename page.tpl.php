<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<title><?php print $head_title; ?></title>
<?php print $head; ?>
<?php print $styles; ?>
<!--[if IE]>
<link type="text/css" rel="stylesheet" media="all" href="/<?php print path_to_theme(); ?>/styles/ie.css" />
<![endif]--> 
<?php print $scripts; ?>
</head>
<body>
  
  <div id="header">
  	<h1><a href="<?php print $front_page ?>" title="<?php print $site_name; ?>"><?php print $site_name; ?></a></h1>
  	<img src="<?php print path_to_theme(); ?>/images/crown.png" />
  	<?php if ($site_slogan): ?>
  	  <?php print $site_slogan; ?>
  	<?php endif; ?>
  	<?php if ($primary_links): ?>
      <div id="header-links">
        <?php print theme('links', $primary_links, array('class' => 'links primary-links')) ?>
      </div>
    <?php endif; ?>
  </div>

	<div id="wrapper">

		<div id="content">
      <?php print $help; ?>
			<?php print $tabs; ?>
			<?php print $messages; ?>
			<?php if ($content_top): ?>
			  <?php print $content_top; ?>
			<?php endif; ?>
			<?php print $content; ?>
		</div>

		<?php if ($right): ?>
  		<div id="right">
        <?php print $right; ?>
  		</div>
  	<?php endif; ?>
    <br class="float" /> <!-- hate to do it, but had to make the header negative margin work -->
	</div> <!-- /wrapper -->
	
  <div id="footer">
  	<?php if ($footer_message): ?>
  	  <div id="footer-message">
        <?php print $footer_message; ?>
      </div>s
    <?php endif; ?>
    <?php if ($secondary_links): ?>
      <div id="footer-links">
        <?php print theme('links', $secondary_links, array('class' => 'links secondary-links')) ?>
      </div>
    <?php endif; ?>
    <div class="divider credit">
      <a href="http://royal.theresaanna.com" target="_blank">Drupal theme by theresaanna</a>
    </div>
  </div>

<?php print $closure; ?>
</body>
</html>
