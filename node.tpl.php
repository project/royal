<div id="node-<?php print $node->nid; ?>" class="post node<?php if ($sticky) { print ' sticky'; } ?><?php if (!$status) { print ' node-unpublished'; } ?>">
  <div class="header-wrapper">
    <div class="date">
      <?php print format_date($node->created, 'custom', 'M'); ?><br />
      <span class="day"><?php print format_date($node->created, 'custom', 'd'); ?></span><br />
      <span class="year"><?php print format_date($node->created, 'custom', 'Y'); ?></span>
    </div>
	  <h1 class="title"><a href="<?php print $node_url; ?>" title="<?php print $title; ?>"><?php print $title; ?></a></h1>
	  <div class="byline"><?php print t('A post by ');
	                            if ($name) { print $name; }
	                            print t(' at ');
	                            print format_date($node->created, 'custom', 'g:h A');  ?>
	  </div>
	  <div class="comments">
	    <?php if ($comment_link) { print $comment_link . '.'; } ?> 
	    <?php if ($read_more_link) { print $read_more_link . ' &raquo;'; } ?> 
	    <?php if ($add_comment_link) { print $add_comment_link; } ?>
	  </div>
	  
	</div>

	<div class="entry">
		<?php print $content; ?>
	</div>
	<img src="/<?php print path_to_theme(); ?>/images/divider.png" class="divider" />
	
	<div class="meta"><?php if ($taxonomy && $teaser): ?>
     <?php print $terms; ?><br/><br/>
    <?php endif;?>
  </div>
</div>
